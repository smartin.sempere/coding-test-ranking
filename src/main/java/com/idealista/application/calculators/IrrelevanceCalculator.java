package com.idealista.application.calculators;

import com.idealista.application.model.Ad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Decides if the ad is irrelevant.
 *
 * @author Sergio Martín
 * @since 1.0
 */
@Component
public class IrrelevanceCalculator {

    private final int irrelevanceLimitPoints;

    @Autowired
    public IrrelevanceCalculator(
            @Value("${com.idealista.irrelevance.limit:40}") int irrelevanceLimitPoints
    ) {
        this.irrelevanceLimitPoints = irrelevanceLimitPoints;
    }

    public boolean isIrrelevant(Ad ad) {

        if (ad.getScore() == null) {
            return true;
        }

        return (ad.getScore() < irrelevanceLimitPoints);
    }

}
