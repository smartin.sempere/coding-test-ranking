package com.idealista.application.calculators;

import com.idealista.application.model.Ad;
import com.idealista.application.calculators.completeness.AdCompletenessScoreCalculator;
import com.idealista.application.calculators.description.AdDescriptionScoreCalculator;
import com.idealista.application.calculators.pictures.AdPicturesScoreCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Calculates the score of an Ad.
 *
 * @author Sergio Martín
 * @since 1.0
 */
@Component
public class ScoreCalculator {

    private final AdDescriptionScoreCalculator adDescriptionScoreCalculator;
    private final AdPicturesScoreCalculator adPicturesScoreCalculator;
    private final AdCompletenessScoreCalculator adCompletenessScoreCalculator;

    @Autowired
    public ScoreCalculator(
            AdDescriptionScoreCalculator adDescriptionScoreCalculator,
            AdPicturesScoreCalculator adPicturesScoreCalculator,
            AdCompletenessScoreCalculator adCompletenessScoreCalculator
    ) {
        this.adDescriptionScoreCalculator = adDescriptionScoreCalculator;
        this.adPicturesScoreCalculator = adPicturesScoreCalculator;
        this.adCompletenessScoreCalculator = adCompletenessScoreCalculator;
    }

    public int calculate(Ad ad) {

        int descriptionPoints = adDescriptionScoreCalculator.calculate(ad);
        int picturesPoints = adPicturesScoreCalculator.calculate(ad);
        int completenessPoints = adCompletenessScoreCalculator.calculate(ad);

        int points = descriptionPoints + picturesPoints + completenessPoints;

        points = Math.min(100, points);
        points = Math.max(0, points);

        ad.setScore(points);

        return points;

    }

}
