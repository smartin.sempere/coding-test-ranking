package com.idealista.application.calculators.completeness;

import com.idealista.application.model.Ad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Calculates completeness score of an Ad.
 *
 * @author Sergio Martín
 * @since 1.0
 */
@Component
public class AdCompletenessScoreCalculator {

    private int points;

    @Autowired
    public AdCompletenessScoreCalculator(
            @Value("${com.idealista.completeness.points:40}") int points
    ) {
        this.points = points;
    }

    public int calculate(Ad ad) {
        return (ad.isComplete()) ? points : 0;
    }

}
