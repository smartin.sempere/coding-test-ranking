package com.idealista.application.calculators.description;

import com.idealista.application.model.Ad;
import com.idealista.application.model.ChaletAd;
import com.idealista.application.model.FlatAd;
import com.idealista.application.model.GarageAd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Calculates description score of an Ad.
 *
 * @author Sergio Martín
 * @since 1.0
 */
@Component
public class AdDescriptionScoreCalculator {

    private final AwardedWordsCounter awardedWordsCounter;
    private final WordRetriever wordRetriever;

    private int hasDescriptionPoints;
    private int awardedWordPoints;

    @Autowired
    public AdDescriptionScoreCalculator(
            @Value("${com.idealista.description.exists:5}") int hasDescriptionPoints,
            @Value("${com.idealista.description.awardedword:5}") int awardedWordPoints
    ) {
        this.awardedWordsCounter = new AwardedWordsCounter();
        this.wordRetriever = new WordRetriever();
        this.hasDescriptionPoints = hasDescriptionPoints;
        this.awardedWordPoints = awardedWordPoints;
    }

    public int calculate(Ad ad) {

        if (ad instanceof FlatAd) {
            return calculate((FlatAd) ad);
        }

        if (ad instanceof ChaletAd) {
            return calculate((ChaletAd) ad);
        }

        if (ad instanceof GarageAd) {
            return calculate((GarageAd) ad);
        }

        return basePoints(ad);
    }

    private int calculate(FlatAd ad) {

        int points = basePoints(ad);

        String[] words = wordRetriever.getWords(ad.getDescription());

        if (words.length >= 50) {
            points += 30;
        }
        else if (words.length >= 20) {
            points += 10;
        }

        return points;

    }

    private int calculate(ChaletAd ad) {

        int points = basePoints(ad);

        String[] words = wordRetriever.getWords(ad.getDescription());

        if (words.length > 50) {
            points += 20;
        }

        return points;

    }

    public int calculate(GarageAd ad) {
        return basePoints(ad);
    }

    private int basePoints(Ad ad) {

        if (ad.getDescription() == null) {
            return 0;
        }

        int descriptionExistsPoints = calculateDescriptionExistsPoints(ad);
        int awardedWordsPoints = calculateAwardedWordsPoints(ad);

        return descriptionExistsPoints + awardedWordsPoints;

    }

    private int calculateDescriptionExistsPoints(Ad ad) {
        return (ad.hasDescription()) ? hasDescriptionPoints : 0;
    }

    private int calculateAwardedWordsPoints(Ad ad) {
        return awardedWordsCounter.count(ad.getDescription()) * awardedWordPoints;
    }

}
