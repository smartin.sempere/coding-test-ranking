package com.idealista.application.calculators.description;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Counts how many awarded words are in a description.
 *
 * @author Sergio Martín
 * @since 1.0
 */
public class AwardedWordsCounter {

    private static List<String> AWARDED_WORDS = Arrays.asList("luminoso", "nuevo", "céntrico", "reformado", "ático");

    public int count(String description) {

        List<String> lowerCaseWords = getLowerCaseWords(description);

        int count = 0;

        for (String word : AWARDED_WORDS) {
            if (lowerCaseWords.contains(word)) {
                count++;
            }
        }

        return count;

    }

    private List<String> getLowerCaseWords(String description) {

        List<String> loweCaseWords = new ArrayList<>();

        WordRetriever wordRetriever = new WordRetriever();
        String[] words = wordRetriever.getWords(description);

        for (String word : words) {
            loweCaseWords.add(word.toLowerCase());
        }

        return loweCaseWords;

    }

}
