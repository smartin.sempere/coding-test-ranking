package com.idealista.application.calculators.description;

/**
 * Get all the words in a description.
 *
 * @author Sergio Martín
 * @since 1.0
 */
public class WordRetriever {

    public String[] getWords(String description) {

        if (description == null || description.isEmpty()) {
            return new String[0];
        }

        return description.split("(?U)\\W+");

    }

}
