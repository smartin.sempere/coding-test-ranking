package com.idealista.application.calculators.pictures;

import com.idealista.application.model.Ad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Calculates pictures score of an Ad.
 *
 * @author Sergio Martín
 * @since 1.0
 */
@Component
public class AdPicturesScoreCalculator {

    private int noPicturePoints;
    private int sdPicturePoints;
    private int hdPicturePoints;

    @Autowired
    public AdPicturesScoreCalculator(
            @Value("${com.idealista.completeness.points:-10}") int noPicturePoints,
            @Value("${com.idealista.completeness.points:10}") int sdPicturePoints,
            @Value("${com.idealista.completeness.points:20}") int hdPicturePoints
    ) {
        this.noPicturePoints = noPicturePoints;
        this.sdPicturePoints = sdPicturePoints;
        this.hdPicturePoints = hdPicturePoints;
    }

    public int calculate(Ad ad) {

        if (ad.getNumPictures() == 0) {
            return noPicturePoints;
        }

        return (ad.getNumSdPictures() * sdPicturePoints) + (ad.getNumHdPictures() * hdPicturePoints);

    }

}
