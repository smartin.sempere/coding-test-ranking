package com.idealista.application.mappers;

import com.idealista.application.model.Ad;
import com.idealista.application.model.Picture;
import com.idealista.infrastructure.api.PublicAd;

import java.util.List;
import java.util.stream.Collectors;

public class AdToPublicAdMapper {

    public static AdToPublicAdMapper aPublicAd() {
        return new AdToPublicAdMapper();
    }

	public PublicAd map(Ad ad) {

        List<String> pictures = ad.getPictures().stream()
                .map(Picture::getUrl)
                .collect(Collectors.toList());

        PublicAd publicAd = new PublicAd();
        publicAd.setId(ad.getId());
        publicAd.setTypology(ad.getTypology());
        publicAd.setDescription(ad.getDescription());
        publicAd.setPictureUrls(pictures);
        publicAd.setHouseSize(ad.getHouseSize());
        publicAd.setGardenSize(ad.getGardenSize());

        return publicAd;

	}

}
