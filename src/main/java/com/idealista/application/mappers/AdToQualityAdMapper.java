package com.idealista.application.mappers;

import com.idealista.application.model.Ad;
import com.idealista.application.model.Picture;
import com.idealista.infrastructure.api.QualityAd;

import java.util.List;
import java.util.stream.Collectors;

public class AdToQualityAdMapper {

    public static AdToQualityAdMapper aQualityAd() {
        return new AdToQualityAdMapper();
    }

	public QualityAd map(Ad ad) {

        List<String> pictures = ad.getPictures().stream()
                .map(Picture::getUrl)
                .collect(Collectors.toList());

        QualityAd qualityAd = new QualityAd();
        qualityAd.setId(ad.getId());
        qualityAd.setTypology(ad.getTypology());
        qualityAd.setDescription(ad.getDescription());
        qualityAd.setPictureUrls(pictures);
        qualityAd.setHouseSize(ad.getHouseSize());
        qualityAd.setGardenSize(ad.getGardenSize());
        qualityAd.setScore(ad.getScore());
        qualityAd.setIrrelevantSince(ad.getIrrelevantSince());

        return qualityAd;

	}

}
