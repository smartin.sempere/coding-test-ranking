package com.idealista.application.mappers;

import com.idealista.application.model.*;
import com.idealista.infrastructure.persistence.AdVO;
import com.idealista.infrastructure.persistence.InMemoryPersistence;
import com.idealista.infrastructure.persistence.PictureVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AdVOToAdMapper {

    private final InMemoryPersistence inMemoryPersistence;

    @Autowired
    public AdVOToAdMapper(InMemoryPersistence inMemoryPersistence) {
        this.inMemoryPersistence = inMemoryPersistence;
    }

    public Ad map(AdVO adVO) {

        List<Picture> pictures = inMemoryPersistence.getPictures(adVO.getPictures())
                .stream()
                .map(this::map)
                .collect(Collectors.toList());

        return AdBuilder.anAd()
                .withId(adVO.getId())
                .withTypology(adVO.getTypology())
                .withDescription(adVO.getDescription())
                .withPictures(pictures)
                .withHouseSize(adVO.getHouseSize())
                .withGardenSize(adVO.getGardenSize())
                .withScore(adVO.getScore())
                .withIrrelevantSince(adVO.getIrrelevantSince())
                .build();

	}

	private Picture map(PictureVO pictureVO) {
        return new Picture(pictureVO.getId(), pictureVO.getUrl(), pictureVO.getQuality());
    }

}
