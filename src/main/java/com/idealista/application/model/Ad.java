package com.idealista.application.model;

import java.util.Date;
import java.util.List;

public abstract class Ad {

    protected Integer id;
    protected String typology;
    protected String description;
    protected List<Picture> pictures;
    protected Integer houseSize;
    protected Integer gardenSize;
    protected Integer score;
    protected Date irrelevantSince;

    public Integer getId() {
        return id;
    }

    public String getTypology() {
        return typology;
    }

    public String getDescription() {
        return description;
    }

    public boolean hasDescription() {
        return description != null && !description.isEmpty();
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public Integer getNumPictures() {
        return getNumSdPictures() + getNumHdPictures();
    }

    public Integer getNumSdPictures() {
        return Math.toIntExact(
                pictures.stream()
                        .filter(picture -> "SD".equals(picture.getQuality()))
                        .count()
        );
    }

    public Integer getNumHdPictures() {
        return Math.toIntExact(
                pictures.stream()
                        .filter(picture -> "HD".equals(picture.getQuality()))
                        .count()
        );
    }

    public Integer getHouseSize() {
        return houseSize;
    }

    public Integer getGardenSize() {
        return gardenSize;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public boolean isIrrelevant() {
        return irrelevantSince != null;
    }

    public Date getIrrelevantSince() {
        return irrelevantSince;
    }

    public abstract boolean isComplete();

}
