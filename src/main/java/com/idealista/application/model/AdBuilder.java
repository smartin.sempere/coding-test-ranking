package com.idealista.application.model;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class AdBuilder {

    private Integer id;
    private String typology;
    private String description;
    private List<Picture> pictures = Collections.emptyList();
    private Integer houseSize;
    private Integer gardenSize;
    private Integer score;
    private Date irrelevantSince;

    private AdBuilder() {

    }

    public static AdBuilder anAd() {
        return new AdBuilder();
    }

    public static AdBuilder aChaletAd() {
        AdBuilder adBuilder = new AdBuilder();
        adBuilder.withTypology("CHALET");
        return adBuilder;
    }

    public static AdBuilder aGarageAd() {
        AdBuilder adBuilder = new AdBuilder();
        adBuilder.withTypology("GARAGE");
        return adBuilder;
    }

    public static AdBuilder aFlatAd() {
        AdBuilder adBuilder = new AdBuilder();
        adBuilder.withTypology("FLAT");
        return adBuilder;
    }

    public AdBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public AdBuilder withTypology(String typology) {
        this.typology = typology;
        return this;
    }

    public AdBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public AdBuilder withPictures(List<Picture> pictures) {
        this.pictures = pictures;
        return this;
    }

    public AdBuilder withHouseSize(Integer houseSize) {
        this.houseSize = houseSize;
        return this;
    }

    public AdBuilder withGardenSize(Integer gardenSize) {
        this.gardenSize = gardenSize;
        return this;
    }

    public AdBuilder withScore(Integer score) {
        this.score = score;
        return this;
    }

    public AdBuilder withIrrelevantSince(Date irrelevantSince) {
        this.irrelevantSince = irrelevantSince;
        return this;
    }

    public Ad build() {

        Ad ad;

        if ("CHALET".equals(typology)) {
            ad = new ChaletAd();
        }
        else if ("GARAGE".equals(typology)) {
            ad = new GarageAd();
        }
        else {
            ad = new FlatAd();
        }

        ad.id = this.id;
        ad.typology = this.typology;
        ad.description = this.description;
        ad.pictures = this.pictures;
        ad.houseSize = this.houseSize;
        ad.gardenSize = this.gardenSize;
        ad.score = this.score;
        ad.irrelevantSince = this.irrelevantSince;
        return ad;
    }

}
