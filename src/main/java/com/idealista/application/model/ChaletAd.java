package com.idealista.application.model;

public class ChaletAd extends Ad {

    public boolean isComplete() {
        return getDescription() != null && getNumPictures() != 0 && getHouseSize() != null && getGardenSize() != null;
    }

}
