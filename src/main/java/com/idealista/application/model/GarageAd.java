package com.idealista.application.model;

public class GarageAd extends Ad {

    public boolean isComplete() {
        return getNumPictures() != 0;
    }

}
