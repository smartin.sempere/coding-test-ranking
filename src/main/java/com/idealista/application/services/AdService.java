package com.idealista.application.services;

import com.idealista.application.model.Ad;

import java.util.List;

public interface AdService {

	List<Ad> getAds(Boolean filterIrrelevantAds);

	void calculateScores();

}
