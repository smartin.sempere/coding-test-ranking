package com.idealista.application.services;

import com.idealista.application.mappers.AdVOToAdMapper;
import com.idealista.application.model.Ad;
import com.idealista.application.calculators.IrrelevanceCalculator;
import com.idealista.application.calculators.ScoreCalculator;
import com.idealista.infrastructure.persistence.AdVO;
import com.idealista.infrastructure.persistence.InMemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

@Service
public class AdServiceImpl implements AdService {

    private final Logger logger = LoggerFactory.getLogger(AdServiceImpl.class);

	private InMemoryPersistence repository;
	private ScoreCalculator scoreCalculator;
    private IrrelevanceCalculator irrelevanceCalculator;

    private AdVOToAdMapper adVOToAdMapper;

	@Autowired
	public AdServiceImpl(
            InMemoryPersistence repository,
            ScoreCalculator scoreCalculator,
            IrrelevanceCalculator irrelevanceCalculator,
            AdVOToAdMapper adVOToAdMapper
	) {
		this.repository = repository;
		this.scoreCalculator = scoreCalculator;
        this.irrelevanceCalculator = irrelevanceCalculator;
        this.adVOToAdMapper = adVOToAdMapper;
	}

    /**
     * Retrieves ads which score has been calculated. If the ad has no score it will be discarded.
     *
     * @param filterIrrelevantAds if true none of irrelevant ads will be retrieved
     * @return ads sorted by descending score
     */
    @Override
    public List<Ad> getAds(Boolean filterIrrelevantAds) {

        return repository.getAds().stream()
                .map((adVO) -> adVOToAdMapper.map(adVO))
                .filter((ad) -> ad.getScore() != null)
                .filter((ad) -> !filterIrrelevantAds || !ad.isIrrelevant())
                .sorted(comparing(Ad::getScore).reversed())
                .collect(toList());
    }

    /**
     * Calculates ad scores
     */
    @Override
	public void calculateScores() {

		List<AdVO> adsVO = repository.getAds();

        logger.debug("Calculating score for {} ad ", adsVO.size());

		for (AdVO adVO : adsVO) {
            calculateScore(adVO);
		}

        logger.debug("Calculated scores ");

	}

	private void calculateScore(AdVO adVO) {

        logger.debug("Calculating score for ad " + adVO.getId());

        Ad ad = adVOToAdMapper.map(adVO);

        int id = ad.getId();
        int score = scoreCalculator.calculate(ad);
        boolean isIrrelevant = irrelevanceCalculator.isIrrelevant(ad);

        logger.debug("Ad {} score: {} -> irrelevant ({})", adVO.getId(), score, isIrrelevant);

        repository.updateAdScore(id, score, isIrrelevant);

    }

}
