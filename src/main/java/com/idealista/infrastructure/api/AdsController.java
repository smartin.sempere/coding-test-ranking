package com.idealista.infrastructure.api;

import java.util.List;
import java.util.stream.Collectors;

import com.idealista.application.services.AdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.idealista.application.mappers.AdToPublicAdMapper.aPublicAd;
import static com.idealista.application.mappers.AdToQualityAdMapper.aQualityAd;

@RestController
@RequestMapping("/ads")
public class AdsController {

    private AdService adService;

    @Autowired
    public AdsController(AdService adService) {
        this.adService = adService;
    }

    @GetMapping(value = "/quality-listing", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<QualityAd>> qualityListing() {

        List<QualityAd> qualityAds = adService.getAds(false)
                .stream()
                .map(ad -> aQualityAd().map(ad))
                .collect(Collectors.toList());

        return ResponseEntity.ok(qualityAds);
    }

    @GetMapping(value = "/public-listing", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<PublicAd>> publicListing() {

        List<PublicAd> publicAds = adService.getAds(true)
                .stream()
                .map(ad -> aPublicAd().map(ad))
                .collect(Collectors.toList());

        return ResponseEntity.ok(publicAds);
    }

    @PutMapping(value="/calculate-score")
    public ResponseEntity<Void> calculateScore() {

        adService.calculateScores();

        return ResponseEntity.noContent().build();
    }

}
