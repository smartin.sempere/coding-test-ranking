package com.idealista.application.calculators;

import com.idealista.application.model.Picture;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PicturesLists {

    private final static Picture HQ_PICTURE_1 = new Picture(1, "", "HD");
    private final static Picture HQ_PICTURE_2 = new Picture(2, "", "HD");
    private final static Picture SQ_PICTURE_1 = new Picture(3, "", "SD");

    public final static List<Picture> ONE_HQ_PICTURE_LIST = Collections.singletonList(HQ_PICTURE_1);
    public final static List<Picture> TWO_HQ_PICTURE_LIST = Arrays.asList(HQ_PICTURE_1, HQ_PICTURE_2);
    public final static List<Picture> TWO_HQ_PICTURE_AND_ONE_SQ_PICTURE_LIST = Arrays.asList(HQ_PICTURE_1, HQ_PICTURE_2, SQ_PICTURE_1);

}
