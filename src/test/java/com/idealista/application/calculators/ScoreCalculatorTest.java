package com.idealista.application.calculators;

import com.idealista.application.model.Ad;
import com.idealista.application.model.AdBuilder;
import com.idealista.application.calculators.completeness.AdCompletenessScoreCalculator;
import com.idealista.application.calculators.description.AdDescriptionScoreCalculator;
import com.idealista.application.calculators.pictures.AdPicturesScoreCalculator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class ScoreCalculatorTest {

    private final static int HAS_DESCRIPTION_POINTS = 5;
    private final static int AWARDED_WORD_POINTS = 5;

    private final static int COMPLETENESS_POINTS = 40;

    private final static int NO_PICTURE_POINTS = -10;
    private final static int LQ_PICTURE_POINTS = 10;
    private final static int HQ_PICTURE_POINTS = 20;

    private ScoreCalculator scoreCalculator;

    @Before
    public void init() {
        this.scoreCalculator = new ScoreCalculator(
                new AdDescriptionScoreCalculator(HAS_DESCRIPTION_POINTS, AWARDED_WORD_POINTS),
                new AdPicturesScoreCalculator(NO_PICTURE_POINTS, LQ_PICTURE_POINTS, HQ_PICTURE_POINTS),
                new AdCompletenessScoreCalculator(COMPLETENESS_POINTS)
        );
    }

    @Test
    public void adWithoutPictures() {

        Ad ad = AdBuilder.aGarageAd()
                .build();

        int score = scoreCalculator.calculate(ad);

        Assert.assertEquals(0, score);

    }

    @Test
    public void adWith2HQPictures() {

        Ad ad = AdBuilder.aGarageAd()
                .withPictures(PicturesLists.TWO_HQ_PICTURE_LIST)
                .build();

        int score = scoreCalculator.calculate(ad);

        Assert.assertEquals(80, score);

    }

    @Test
    public void adWith2HQPicturesAnd1SQPicture() {

        Ad ad = AdBuilder.aGarageAd()
                .withPictures(PicturesLists.TWO_HQ_PICTURE_AND_ONE_SQ_PICTURE_LIST)
                .build();

        int score = scoreCalculator.calculate(ad);

        Assert.assertEquals(90, score);

    }

}
