package com.idealista.application.calculators.completeness;

import com.idealista.application.calculators.PicturesLists;
import com.idealista.application.model.Ad;
import com.idealista.application.model.AdBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
public class ChaletAdCompletenessScoreCalculatorTest {

    private final static int POINTS = 40;

    private final static String DESCRIPTION = "Description with content";
    private final static Integer HOUSE_SIZE = 120;
    private final static Integer GARDEN_SIZE = 200;

    private AdCompletenessScoreCalculator adCompletenessScoreCalculator;

    @Before
    public void init() {
        this.adCompletenessScoreCalculator = new AdCompletenessScoreCalculator(POINTS);
    }

    @Test
    public void flatAdWithoutDescription() {

        Ad ad = AdBuilder.aChaletAd()
                .withPictures(PicturesLists.ONE_HQ_PICTURE_LIST)
                .withHouseSize(HOUSE_SIZE)
                .withGardenSize(GARDEN_SIZE)
                .build();

        int score = adCompletenessScoreCalculator.calculate(ad);

        Assert.assertEquals(0, score);

    }

    @Test
    public void flatAdWithoutPictures() {

        Ad ad = AdBuilder.aChaletAd()
                .withDescription(DESCRIPTION)
                .withHouseSize(HOUSE_SIZE)
                .withGardenSize(GARDEN_SIZE)
                .build();

        int score = adCompletenessScoreCalculator.calculate(ad);

        Assert.assertEquals(0, score);

    }

    @Test
    public void flatAdWithoutHouseSize() {

        Ad ad = AdBuilder.aChaletAd()
                .withDescription(DESCRIPTION)
                .withPictures(PicturesLists.ONE_HQ_PICTURE_LIST)
                .withGardenSize(GARDEN_SIZE)
                .build();

        int score = adCompletenessScoreCalculator.calculate(ad);

        Assert.assertEquals(0, score);

    }

    @Test
    public void flatAdWithoutGardenSize() {

        Ad ad = AdBuilder.aChaletAd()
                .withDescription(DESCRIPTION)
                .withPictures(PicturesLists.ONE_HQ_PICTURE_LIST)
                .withHouseSize(HOUSE_SIZE)
                .build();

        int score = adCompletenessScoreCalculator.calculate(ad);

        Assert.assertEquals(0, score);

    }

    @Test
    public void flatAdComplete() {

        Ad ad = AdBuilder.aChaletAd()
                .withDescription(DESCRIPTION)
                .withPictures(PicturesLists.ONE_HQ_PICTURE_LIST)
                .withHouseSize(HOUSE_SIZE)
                .withGardenSize(GARDEN_SIZE)
                .build();

        int score = adCompletenessScoreCalculator.calculate(ad);

        Assert.assertEquals(POINTS, score);

    }

}
