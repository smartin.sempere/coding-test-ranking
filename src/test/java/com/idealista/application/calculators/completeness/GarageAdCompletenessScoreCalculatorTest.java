package com.idealista.application.calculators.completeness;

import com.idealista.application.calculators.PicturesLists;
import com.idealista.application.model.Ad;
import com.idealista.application.model.AdBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class GarageAdCompletenessScoreCalculatorTest {

    private final static int COMPLETENESS_POINTS = 40;

    private AdCompletenessScoreCalculator adCompletenessScoreCalculator;

    @Before
    public void init() {
        this.adCompletenessScoreCalculator = new AdCompletenessScoreCalculator(COMPLETENESS_POINTS);
    }

    @Test
    public void withoutPictures() {

        Ad ad = AdBuilder.aGarageAd().build();

        int score = adCompletenessScoreCalculator.calculate(ad);

        Assert.assertEquals(0, score);

    }

    @Test
    public void withPictures() {

        Ad ad = AdBuilder.aGarageAd()
                .withPictures(PicturesLists.ONE_HQ_PICTURE_LIST)
                .build();

        int score = adCompletenessScoreCalculator.calculate(ad);

        Assert.assertEquals(COMPLETENESS_POINTS, score);

    }

}
