package com.idealista.application.calculators.description;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class AwardedWordRetrieverTest {

    @Parameter(value = 0)
    public String description;

    @Parameter(value = 1)
    public int expectedNumWords;

    @Parameters(name = "{index}: awarded words({0}) = {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {null, 0},
                {"", 0},
                {"Pisazo", 0},
                {"Este piso es una ganga, compra, compra, COMPRA!!!!!", 0},
                {"Ático céntrico muy luminoso y recién reformado, parece nuevo", 5},
                {"Nuevo ático céntrico recién reformado. No deje pasar la oportunidad y adquiera este ático de lujo", 4},
                {"Matemático no debe contabilizarse, es una subcadena", 0}
        });
    }

    @Test
    public void test() {
        AwardedWordsCounter awardedWordsCounter = new AwardedWordsCounter();
        int numWords = awardedWordsCounter.count(description);
        Assert.assertEquals(expectedNumWords, numWords);
    }

}
