package com.idealista.application.calculators.description;

import com.idealista.application.model.Ad;
import com.idealista.application.model.AdBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class ChaletAdDescriptionScoreCalculatorTest {

    private final static int HAS_DESCRIPTION_POINTS = 5;
    private final static int AWARDED_WORD_POINTS = 5;

    @Parameterized.Parameter(value = 0)
    public String description;

    @Parameterized.Parameter(value = 1)
    public int expectedNumWords;

    @Parameterized.Parameters(name = "{index}: rank({0}) = {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {null, 0},
                {"", 0},
                {"Without awarded words", 5},
                {"Four awarded words (Nuevo ático céntrico reformado)", 25},
                {"Five awarded words, some repeated (Nuevo ático céntrico reformado luminoso luminoso)", 30},
                {"1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50", 5},
                {"1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51", 25},
                {"1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 nuevo", 30}
        });
    }

    private AdDescriptionScoreCalculator adDescriptionScoreCalculator;

    @Before
    public void init() {
        this.adDescriptionScoreCalculator = new AdDescriptionScoreCalculator(HAS_DESCRIPTION_POINTS, AWARDED_WORD_POINTS);
    }

    @Test
    public void test() {

        Ad ad = AdBuilder.aChaletAd()
                .withDescription(description)
                .build();

        int points = adDescriptionScoreCalculator.calculate(ad);

        Assert.assertEquals(expectedNumWords, points);

    }

}
