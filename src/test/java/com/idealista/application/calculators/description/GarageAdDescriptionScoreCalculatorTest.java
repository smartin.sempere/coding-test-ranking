package com.idealista.application.calculators.description;

import com.idealista.application.model.Ad;
import com.idealista.application.model.AdBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class GarageAdDescriptionScoreCalculatorTest {

    private final static int HAS_DESCRIPTION_POINTS = 5;
    private final static int AWARDED_WORD_POINTS = 5;

    @Parameterized.Parameter(value = 0)
    public String description;

    @Parameterized.Parameter(value = 1)
    public int expectedNumWords;

    @Parameterized.Parameters(name = "{index}: rank({0}) = {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {null, 0},
                {"", 0},
                {"Without awarded words", 5},
                {"Four awarded words (Nuevo ático céntrico reformado)", 25},
                {"Five awarded words, some repeated (Nuevo ático céntrico reformado luminoso luminoso)", 30},
                {"Without awarded words but very large with a lot of words! Result is the same as if there were not an awarded word", 5}
        });
    }

    private AdDescriptionScoreCalculator adDescriptionScoreCalculator;

    @Before
    public void init() {
        this.adDescriptionScoreCalculator = new AdDescriptionScoreCalculator(HAS_DESCRIPTION_POINTS, AWARDED_WORD_POINTS);
    }

    @Test
    public void test() {

        Ad ad = AdBuilder.aGarageAd()
                .withDescription(description)
                .build();

        int points = adDescriptionScoreCalculator.calculate(ad);

        Assert.assertEquals(expectedNumWords, points);

    }

}
