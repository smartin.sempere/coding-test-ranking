package com.idealista.application.calculators.description;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Parameterized.Parameter;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class WordRetrieverTest {

    @Parameter(value = 0)
    public String description;

    @Parameter(value = 1)
    public int expectedNumWords;

    @Parameters(name = "{index}: words({0}) = {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {null, 0},
                {"", 0},
                {"Este piso es una ganga, compra, compra, COMPRA!!!!!", 8},
                {"Nuevo ático céntrico recién reformado. No deje pasar la oportunidad y adquiera este ático de lujo", 16},
                {"Ático céntrico muy luminoso y recién reformado, parece nuevo", 9},
                {"Pisazo", 1},
                {"Garaje en el centro de Albacete", 6},
                {"Maravilloso chalet situado en lAs afueras de un pequeño pueblo rural. El entorno es espectacular, las vistas magníficas. ¡Cómprelo ahora!", 20},
                {"Comas sin espacios,funciona", 4},
                {"Parentesis(funciona)", 2}
        });
    }

    @Test
    public void test() {
        WordRetriever wordRetriever = new WordRetriever();
        String[] words = wordRetriever.getWords(description);
        int numWords = words.length;
        Assert.assertEquals(expectedNumWords, numWords);
    }

}
