package com.idealista.application.calculators.pictures;

import com.idealista.application.calculators.PicturesLists;
import com.idealista.application.model.Ad;
import com.idealista.application.model.AdBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class AdPictureScoreCalculatorTest {

    private final static int NO_PICTURE_POINTS = -10;
    private final static int LQ_PICTURE_POINTS = 10;
    private final static int HQ_PICTURE_POINTS = 20;

    private AdPicturesScoreCalculator adPicturesScoreCalculator;

    @Before
    public void init() {
        this.adPicturesScoreCalculator = new AdPicturesScoreCalculator(NO_PICTURE_POINTS, LQ_PICTURE_POINTS, HQ_PICTURE_POINTS);
    }

    @Test
    public void adWithoutPictures() {

        Ad ad = AdBuilder.aGarageAd()
                .build();

        int score = adPicturesScoreCalculator.calculate(ad);

        Assert.assertEquals(-10, score);

    }

    @Test
    public void adWith2HQPictures() {

        Ad ad = AdBuilder.aGarageAd()
                .withPictures(PicturesLists.TWO_HQ_PICTURE_LIST)
                .build();

        int score = adPicturesScoreCalculator.calculate(ad);

        Assert.assertEquals(40, score);

    }

    @Test
    public void adWith2HQPicturesAnd1LQPicture() {

        Ad ad = AdBuilder.aGarageAd()
                .withPictures(PicturesLists.TWO_HQ_PICTURE_AND_ONE_SQ_PICTURE_LIST)
                .build();

        int score = adPicturesScoreCalculator.calculate(ad);

        Assert.assertEquals(50, score);

    }

}
