package com.idealista.application.mappers;

import com.idealista.application.model.Ad;
import com.idealista.application.model.AdBuilder;
import com.idealista.infrastructure.api.PublicAd;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;

@RunWith(SpringJUnit4ClassRunner.class)
public class AdToPublicAdMapperTest {

    @Test
    public void adWithoutPictures() {

        Integer id = 1;
        String typology = "CHALET";
        String description = "Description";
        Integer houseSize = 120;
        Integer gardenSize = 180;

        Ad ad = AdBuilder.anAd()
                .withId(id)
                .withTypology(typology)
                .withDescription(description)
                .withPictures(Collections.emptyList())
                .withHouseSize(houseSize)
                .withGardenSize(gardenSize)
                .build();

        PublicAd publicAd = AdToPublicAdMapper
                .aPublicAd()
                .map(ad);

        Assert.assertEquals(id, publicAd.getId());
        Assert.assertEquals(typology, publicAd.getTypology());
        Assert.assertEquals(description, publicAd.getDescription());
        Assert.assertEquals(houseSize, publicAd.getHouseSize());
        Assert.assertEquals(gardenSize, publicAd.getGardenSize());

    }

}
