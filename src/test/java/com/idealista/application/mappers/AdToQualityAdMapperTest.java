package com.idealista.application.mappers;

import com.idealista.application.model.Ad;
import com.idealista.application.model.AdBuilder;
import com.idealista.infrastructure.api.PublicAd;
import com.idealista.infrastructure.api.QualityAd;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
public class AdToQualityAdMapperTest {

    @Test
    public void adWithoutPictures() {

        Integer id = 1;
        String typology = "CHALET";
        String description = "Description";
        Integer houseSize = 120;
        Integer gardenSize = 180;
        Integer score = 20;
        Date irrelevantSince = new Date();

        Ad ad = AdBuilder.anAd()
                .withId(id)
                .withTypology(typology)
                .withDescription(description)
                .withPictures(Collections.emptyList())
                .withHouseSize(houseSize)
                .withGardenSize(gardenSize)
                .withScore(score)
                .withIrrelevantSince(irrelevantSince)
                .build();

        QualityAd qualityAd = AdToQualityAdMapper
                .aQualityAd()
                .map(ad);

        Assert.assertEquals(id, qualityAd.getId());
        Assert.assertEquals(typology, qualityAd.getTypology());
        Assert.assertEquals(description, qualityAd.getDescription());
        Assert.assertEquals(houseSize, qualityAd.getHouseSize());
        Assert.assertEquals(gardenSize, qualityAd.getGardenSize());
        Assert.assertEquals(score, qualityAd.getScore());
        Assert.assertEquals(irrelevantSince, qualityAd.getIrrelevantSince());

    }

}
