package com.idealista.application.mappers;

import com.idealista.application.model.Ad;
import com.idealista.infrastructure.persistence.AdVO;
import com.idealista.infrastructure.persistence.InMemoryPersistence;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
public class AdVOToAdMapperTest {

    private AdVOToAdMapper adVOToAdMapper;

    @Mock
    public InMemoryPersistence inMemoryPersistence;

    @Before
    public void init() {
        this.adVOToAdMapper = new AdVOToAdMapper(inMemoryPersistence);
    }

    @Test
    public void adWithoutPictures() {

        Integer id = 1;
        String typology = "CHALET";
        String description = "Description";
        Integer houseSize = 120;
        Integer gardenSize = 180;
        Integer score = 30;
        Date irrelevantSince = new Date();

        AdVO adVO = new AdVO(id, typology, description, Collections.emptyList(), houseSize, gardenSize, score, irrelevantSince);

        Ad ad = this.adVOToAdMapper.map(adVO);

        Assert.assertEquals(id, ad.getId());
        Assert.assertEquals(typology, ad.getTypology());
        Assert.assertEquals(description, ad.getDescription());
        Assert.assertEquals(houseSize, ad.getHouseSize());
        Assert.assertEquals(gardenSize, ad.getGardenSize());
        Assert.assertEquals(score, ad.getScore());
        Assert.assertEquals(irrelevantSince, ad.getIrrelevantSince());

    }

}
